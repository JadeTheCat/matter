/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.util;

import me.jadethecat.matter.Matter;
import me.jadethecat.matter.api.MatterInitializer;
import me.jadethecat.matter.api.material.MaterialBit;
import me.jadethecat.matter.api.material.MaterialBitCallback;
import me.jadethecat.matter.api.material.MaterialRequestManager;
import me.jadethecat.matter.material.Materials;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;

public class TestRequester implements MatterInitializer {
    private static final String MODID = "testmod";
    @Override
    public void initRequests(MaterialRequestManager manager) {
        Matter.LOGGER.warn("Warning: TestRequester running. If this is not a dev environment, please notify the author!");
        manager.request(MODID, Materials.Ids.COAL, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK);
        manager.request(MODID, Materials.Ids.IRON, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.GOLD, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.DIAMOND, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.QUARTZ, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.REDSTONE, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.LAPIS, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.EMERALD, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);

        manager.request(MODID, Materials.Ids.COPPER, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.ALUMINUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.TIN, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.SILVER, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.LEAD, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.ZINC, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.TUNGSTEN, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.TITANIUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.URANIUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.COBALT, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.OSMIUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.PLATINUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.PALLADIUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.IRIDIUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);


        manager.request(MODID, Materials.Ids.RUBY, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.SAPPHIRE, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.PERIDOT, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.QUICKSILVER, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.AMETHYST, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.TOPAZ, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);

        manager.request(MODID, Materials.Ids.STEEL, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.BRONZE, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.BRASS, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.ELECTRUM, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);
        manager.request(MODID, Materials.Ids.ROSE_GOLD, MaterialBit.DUST, MaterialBit.MATTER, MaterialBit.NUGGET, MaterialBit.ORE, MaterialBit.NETHER_ORE, MaterialBit.END_ORE, MaterialBit.STORAGE_BLOCK, MaterialBit.GEAR, MaterialBit.PLATE);

        manager.request(MODID, Materials.Ids.COAL_COKE, MaterialBit.MATTER, MaterialBit.STORAGE_BLOCK);
    }

    @Override
    public void initMaterials() {

    }
}
