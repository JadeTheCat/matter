/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.util;

/**
 * Prefixes and Postfixes
 */
public class NameTransformer {
    private String prefix = "";
    private String postfix = "";
    private String nameReplacement = "";

    /**
     * Adds a prefix to the NameTransformer.
     * @param prefix The prefix to add.
     * @return The NameTransformer, with added postfix..
     */
    public NameTransformer withPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    /**
     * Adds a postfix. to the NameTransformer.
     * @param postfix The postfix to add.
     * @return The NameTransformer, with added postfix.
     */
    public NameTransformer withPostfix(String postfix) {
        this.postfix = postfix;
        return this;
    }

    public NameTransformer withNameReplacement(String replacement) {
        this.nameReplacement = replacement;
        return this;
    }

    public String transform(String name) {
        if (!nameReplacement.equals(""))
            name = nameReplacement;
        return prefix+name+postfix;
    }

    // Default Matter NameTransformer
    /**
     * NameTransformer for Ingots (Prefix: none, Postfix: _ingot).
     */
    public static final NameTransformer INGOT_TRANSFORMER = new NameTransformer().withPostfix("_ingot");
    /**
     * NameTransformer for Gems (No NameTransformer).
     */
    public static final NameTransformer GEM_TRANSFORMER = new NameTransformer();

    // Default Ore NameTransformer
    /**
     * NameTransformer for Ores (Prefix: none, Postfix: _ore).
     */
    public static final NameTransformer ORE_TRANSFORMER = new NameTransformer().withPostfix("_ore");
    /**
     * NameTransformer for Nether Ores (Prefix: nether_, Postfix: _ore).
     */
    public static final NameTransformer NETHER_ORE_TRANSFORMER = new NameTransformer().withPrefix("nether_").withPostfix("_ore");
    public static final NameTransformer END_ORE_TRANSFORMER = new NameTransformer().withPrefix("end_").withPostfix("_ore");

    // Default Storage Block NameTransformer
    /**
     * NameTransformer for Storage Blocks (Prefix: none, Postfix: _block).
     */
    public static final NameTransformer STORAGE_BLOCK_TRANSFORMER = new NameTransformer().withPostfix("_block");

    // Default Dust NameTransformer
    /**
     * NameTransformer for Dusts (Prefix: none, Postfix: _dust).
     */
    public static final NameTransformer DUST_TRANSFORMER = new NameTransformer().withPostfix("_dust");

    // Default Nugget NameTransformer
    /**
     * NameTransformer for Nuggets (Prefix: none, Postfix: _nugget).
     */
    public static final NameTransformer NUGGET_TRANSFORMER = new NameTransformer().withPostfix("_nugget");

    // Default Gear NameTransformer
    public static final NameTransformer GEAR_TRANSFORMER = new NameTransformer().withPostfix("_gear");

    // Default Plate NameTransformer
    public static final NameTransformer PLATE_TRANSFORMER = new NameTransformer().withPostfix("_plate");
}
