/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter;

import me.jadethecat.matter.api.MatterInitializer;
import me.jadethecat.matter.api.material.MaterialRegistry;
import me.jadethecat.matter.api.material.MaterialRequestManager;
import me.jadethecat.matter.config.MatterConfig;
import me.jadethecat.matter.material.Materials;
import me.jadethecat.matter.tag.WorldTagReloadListener;
import me.jadethecat.matter.world.MatterOreFeature;
import me.jadethecat.matter.world.OregenResourceListener;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.registry.RegistryEntryAddedCallback;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.decorator.Decorator;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.FeatureConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Matter implements ModInitializer, MatterInitializer {
	public static final String COMMON = "c";
	public static final String MODID = "matter";
	public static final Logger LOGGER = LogManager.getLogger();
	public static MatterConfig CONFIG = new MatterConfig();
	@Override
	public void onInitialize() {
		CONFIG = MatterData.loadConfig();
		Registry.register(Registry.REGISTRIES, MaterialRegistry.MATERIAL_REGISTRY_ID, MaterialRegistry.MATERIAL);
		LOGGER.debug("Running material requests.");
		FabricLoader.getInstance().getEntrypoints(MODID, MatterInitializer.class).forEach((init) -> {
			init.initRequests(MaterialRequestManager.INSTANCE);
		});
		LOGGER.debug("Running material registration.");
		FabricLoader.getInstance().getEntrypoints(MODID, MatterInitializer.class)
				.forEach(MatterInitializer::initMaterials);

		if (!FabricLoader.getInstance().isModLoaded("cotton-resources")) {
			setupBiomeGenerators();
			RegistryEntryAddedCallback.event(Registry.BIOME).register((id, ident, biome) -> setupBiomeGenerator(biome));
			ResourceManagerHelper.get(net.minecraft.resource.ResourceType.SERVER_DATA).registerReloadListener(new WorldTagReloadListener());
			ResourceManagerHelper.get(net.minecraft.resource.ResourceType.SERVER_DATA).registerReloadListener(new OregenResourceListener());
		}
	}

	@Override
	public void initRequests(MaterialRequestManager manager) {

	}

	@Override
	public void initMaterials() {
		Materials.registerAll();
	}

	private static void setupBiomeGenerators() {
		for (Biome biome : Registry.BIOME) {
			setupBiomeGenerator(biome);
		}
	}

	private static void setupBiomeGenerator(Biome biome) {
		biome.addFeature(GenerationStep.Feature.UNDERGROUND_ORES,
				MatterOreFeature.MATTER_ORE
						.configure(FeatureConfig.DEFAULT)
						.createDecoratedFeature(
								Decorator.COUNT_RANGE.configure(new RangeDecoratorConfig(1, 0, 0, 256)
								)
						));
	}
}
