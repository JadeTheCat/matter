/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter;


import blue.endless.jankson.*;
import blue.endless.jankson.api.SyntaxError;
import io.github.cottonmc.jankson.JanksonFactory;
import io.github.cottonmc.staticdata.StaticData;
import io.github.cottonmc.staticdata.StaticDataItem;
import me.jadethecat.matter.api.material.MaterialRequest;
import me.jadethecat.matter.config.MatterConfig;
import me.jadethecat.matter.world.OreGenerationSettings;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.Identifier;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Data handler for matter.
 */
public class MatterData {
    public static final Jankson JANKSON = JanksonFactory.createJankson();

    public static MatterConfig loadConfig() {
        File file = new File(FabricLoader.getInstance().getConfigDirectory(), "matter.json5");

        try {
            JsonObject json = JANKSON.load(file);
            Matter.LOGGER.info("Loading: " + json);
            MatterConfig loading = JANKSON.fromJson(json, MatterConfig.class);
            Matter.LOGGER.info("Loaded Map: " + loading.generators);
            //Manually reload oregen because BiomeSpec and DimensionSpec can be fussy

            JsonObject oregen = json.getObject("generators");
            if (oregen != null) {
                Matter.LOGGER.info("RELOADING " + oregen.size() + " entries");

                for (Map.Entry<String, JsonElement> entry : oregen.entrySet()) {
                    if (entry.getValue() instanceof JsonObject) {
                        OreGenerationSettings settings = OreGenerationSettings.deserialize((JsonObject) entry.getValue());
                        loading.generators.put(entry.getKey(), settings);
                    }
                }
            }
            JsonObject enabledBitElem = json.getObject("enabledBits");
            if (enabledBitElem != null) {
                for (Map.Entry<String, JsonElement> entry : enabledBitElem.entrySet()) {
                    if (entry.getValue() instanceof JsonArray) {
                        Set<String> things = new HashSet<>();
                        for (JsonElement el : ((JsonArray) entry.getValue())) {
                            if (el instanceof JsonPrimitive) {
                                things.add(((JsonPrimitive) el).asString());
                            }
                        }
                        loading.enabledBits.put(entry.getKey(), things);
                    }
                }
            }

            Matter.LOGGER.info("RELOADED Map: " + loading.generators);

            return loading;
        } catch (IOException | SyntaxError e) {
            e.printStackTrace();
            return new MatterConfig();
        }
    }

    /**
     * Get the Data-driven bit requests from static_data
     * @param materialId The Identifier for the material the data is requested for.
     * @return A list of MaterialRequests for the material.
     */
    public static List<MaterialRequest> getStaticRequests(Identifier materialId) {
        Matter.LOGGER.debug("Loading Static Data for {}", materialId.toString());
        Set<StaticDataItem> data = StaticData.getAll(String.format("matter/%s/%s.json5", materialId.getNamespace(), materialId.getPath()));
        List<MaterialRequest> out = new ArrayList<>();
        for (StaticDataItem item : data) {
            try {
                JsonObject json = JANKSON.load(item.createInputStream());
                Matter.LOGGER.debug("Found static request: {}", item.getIdentifier().toString());
                MaterialRequest req = loadRequest(item.getIdentifier().getNamespace(), json);
                out.add(req);
            } catch (IOException | SyntaxError e) {
                Matter.LOGGER.error("Error loading Request {}: {}", item.getIdentifier().toString(), e.getMessage());
            }
        }
        return out;
    }

    private static MaterialRequest loadRequest(String from, JsonObject json) {
        MaterialRequest out = new MaterialRequest(from);
        if (json.containsKey("requestedBits")) {
            JsonElement el = json.get("requestedBits");
            if (el instanceof JsonArray) {
                JsonArray bits = (JsonArray)el;
                for (JsonElement bitElem : bits) {
                    if (bitElem instanceof  JsonPrimitive) {
                        String str = ((JsonPrimitive) bitElem).asString();
                        out.request(new Identifier(str));
                    }
                }
            }
        }
        return out;
    }
}
