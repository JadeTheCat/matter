package me.jadethecat.matter.config;

import me.jadethecat.matter.api.world.OreGenConfig;
import me.jadethecat.matter.world.OreGenerationSettings;
import net.minecraft.util.Identifier;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MatterConfig {
    public boolean override_vanilla_generation = false;
    public Set<String> enabledResources = new HashSet<>();
    public Set<String> disabledResources = new HashSet<>();
    public HashMap<String, OreGenerationSettings> generators = new HashMap<>();
    public HashMap<String, Set<String>> enabledBits = new HashMap<>();
}
