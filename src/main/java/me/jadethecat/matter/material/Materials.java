/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.material;

import me.jadethecat.matter.Matter;
import me.jadethecat.matter.api.material.Material;
import me.jadethecat.matter.api.material.MaterialBit;
import me.jadethecat.matter.api.material.MaterialRegistry;
import me.jadethecat.matter.block.BlockSuppliers;
import me.jadethecat.matter.item.ItemSuppliers;
import me.jadethecat.matter.util.NameTransformer;
import net.minecraft.block.Blocks;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.awt.*;

/**
 * A class containing all of Matter's built-in Materials.
 */
public class Materials {
    public static Material COAL;
    public static Material IRON;
    public static Material GOLD;
    public static Material DIAMOND;
    public static Material QUARTZ;
    public static Material REDSTONE;
    public static Material LAPIS;
    public static Material EMERALD;
    // Here for the future :)
    //public static Material NETHERITE;

    // Metals
    public static final Material COPPER;
    public static final Material TIN;
    public static final Material ALUMINUM;
    public static final Material SILVER;
    public static final Material PLATINUM;
    public static final Material LEAD;
    public static final Material ZINC;
    public static final Material TUNGSTEN;
    public static final Material COBALT;
    public static final Material OSMIUM;
    public static final Material PALLADIUM;
    public static final Material TITANIUM;
    public static final Material URANIUM;
    public static final Material THORIUM;
    public static final Material PLUTONIUM;
    public static final Material IRIDIUM;

    // Gems
    public static final Material RUBY;
    public static final Material SAPPHIRE;
    public static final Material PERIDOT;
    public static final Material QUICKSILVER;
    public static final Material AMETHYST;
    public static final Material TOPAZ;

    // Alloys
    public static final Material STEEL;
    public static final Material BRONZE;
    public static final Material BRASS;
    public static final Material ELECTRUM;
    public static final Material ROSE_GOLD;

    // Other
    public static final Material COAL_COKE;

    private static final Color THORIUM_PARTICLE_COLOR = Color.decode("#E81C6F");
    private static final Color URANIUM_PARTICLE_COLOR = Color.decode("#74D94D");

    private static void register(Identifier id, Material mat) {
        Registry.register(MaterialRegistry.MATERIAL, id, mat);
    }    
    static {
        COAL = new Material.Builder(Ids.COAL)
                .withBit(MaterialBit.MATTER, Items.COAL, NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, Blocks.COAL_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierZeroOre(0, 2),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierZeroOre(0, 2), 
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.COAL_BLOCK, NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .build();
        IRON = new Material.Builder(Ids.IRON)
                .withBit(MaterialBit.MATTER, Items.IRON_INGOT, NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, Items.IRON_NUGGET, NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, Blocks.IRON_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierOneOre(0, 0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierOneOre(0, 0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.IRON_BLOCK, NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        GOLD = new Material.Builder(Ids.GOLD)
                .withBit(MaterialBit.MATTER, Items.GOLD_INGOT, NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, Items.GOLD_NUGGET, NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, Blocks.GOLD_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0, 0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0, 0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.GOLD_BLOCK, NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        DIAMOND = new Material.Builder(Ids.DIAMOND)
                .withBit(MaterialBit.MATTER, Items.DIAMOND, NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, Blocks.DIAMOND_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(3, 7),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(3, 7), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.DIAMOND_BLOCK, NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        QUARTZ = new Material.Builder(Ids.QUARTZ)
                .withBit(MaterialBit.MATTER, Items.QUARTZ, NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierZeroOre(2,5),
                        NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, Blocks.NETHER_QUARTZ_ORE, NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierZeroOre(2, 5),
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.QUARTZ_BLOCK, NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        REDSTONE = new Material.Builder(Ids.REDSTONE)
                .withBit(MaterialBit.ORE, Blocks.REDSTONE_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoLightableOre(1, 5, Color.red),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoLightableOre(1, 5, Color.red),
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.REDSTONE_BLOCK,
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, Items.REDSTONE, NameTransformer.GEM_TRANSFORMER)
                .build();
        LAPIS = new Material.Builder(Ids.LAPIS)
                .withBit(MaterialBit.MATTER, Items.LAPIS_LAZULI, new NameTransformer().withPostfix("_lazuli"))
                .withBit(MaterialBit.ORE, Blocks.LAPIS_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierOneOre(2, 5),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierOneOre(2, 5),
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.LAPIS_BLOCK,
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        EMERALD = new Material.Builder(Ids.EMERALD)
                .withBit(MaterialBit.MATTER, Items.EMERALD, NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, Blocks.EMERALD_ORE, NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(3, 7),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(3, 7),
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, Blocks.EMERALD_BLOCK,
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        // Here for the future :)
        //NETHERITE = new Material(Bits.NETHERITE_BITS, Material.MatterType.INGOT));

        Matter.LOGGER.debug("[Materials] Registering Metal Materials");
        // Metals
        COPPER = new Material.Builder(Ids.COPPER)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierOneOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierOneOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierOneOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        TIN = new Material.Builder(Ids.TIN)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierOneOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierOneOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierOneOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        ALUMINUM = new Material.Builder(Ids.ALUMINUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        SILVER = new Material.Builder(Ids.SILVER)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        PLATINUM = new Material.Builder(Ids.PLATINUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        LEAD = new Material.Builder(Ids.LEAD)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        ZINC = new Material.Builder(Ids.ZINC)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierOneOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierOneOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierOneOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        TUNGSTEN = new Material.Builder(Ids.TUNGSTEN)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        COBALT = new Material.Builder(Ids.COBALT)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        OSMIUM = new Material.Builder(Ids.OSMIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        PALLADIUM = new Material.Builder(Ids.PALLADIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        TITANIUM = new Material.Builder(Ids.TITANIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierThreeOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierThreeOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierThreeOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        URANIUM = new Material.Builder(Ids.URANIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoLightableOre(0,0, URANIUM_PARTICLE_COLOR),
                        NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoLightableOre(0,0, URANIUM_PARTICLE_COLOR),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoLightableOre(0,0, URANIUM_PARTICLE_COLOR),
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        THORIUM = new Material.Builder(Ids.THORIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoLightableOre(0,0, THORIUM_PARTICLE_COLOR),
                        NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoLightableOre(0,0,THORIUM_PARTICLE_COLOR ),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoLightableOre(0,0, THORIUM_PARTICLE_COLOR),
                        NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        IRIDIUM = new Material.Builder(Ids.IRIDIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        PLUTONIUM = new Material.Builder(Ids.PLUTONIUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();

        Matter.LOGGER.debug("[Materials] Registering Gem Materials");
        // Gems
        RUBY = new Material.Builder(Ids.RUBY)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(1, 5),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        SAPPHIRE = new Material.Builder(Ids.SAPPHIRE)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(1, 5),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        PERIDOT = new Material.Builder(Ids.PERIDOT)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(1, 5),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        QUICKSILVER = new Material.Builder(Ids.QUICKSILVER)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(0,0), new NameTransformer()
                        .withPostfix("_ore")
                        .withNameReplacement("cinnabar"))
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(0,0), new NameTransformer()
                        .withPrefix("nether_")
                        .withPostfix("_ore")
                        .withNameReplacement("cinnabar"))
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(0,0), new NameTransformer()
                        .withPrefix("end_")
                        .withPostfix("_ore")
                        .withNameReplacement("cinnabar"))
                .build();
        AMETHYST = new Material.Builder(Ids.AMETHYST)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(1, 5),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        TOPAZ = new Material.Builder(Ids.TOPAZ)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.ORE_TRANSFORMER)
                .withBit(MaterialBit.NETHER_ORE, BlockSuppliers.tierTwoOre(1, 5),
                        NameTransformer.NETHER_ORE_TRANSFORMER)
                .withBit(MaterialBit.END_ORE, BlockSuppliers.tierTwoOre(1, 5), NameTransformer.END_ORE_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();

        Matter.LOGGER.debug("[Materials] Registering Alloy Materials.");
        // Alloys
        STEEL = new Material.Builder(Ids.STEEL)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(), NameTransformer
                        .STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        BRONZE = new Material.Builder(Ids.BRONZE)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(), NameTransformer
                        .STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        BRASS = new Material.Builder(Ids.BRASS)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(), NameTransformer
                        .STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        ELECTRUM = new Material.Builder(Ids.ELECTRUM)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        ROSE_GOLD = new Material.Builder(Ids.ROSE_GOLD)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.INGOT_TRANSFORMER)
                .withBit(MaterialBit.NUGGET, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.NUGGET_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(),
                        NameTransformer.STORAGE_BLOCK_TRANSFORMER)
                .withBit(MaterialBit.DUST, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.DUST_TRANSFORMER)
                .withBit(MaterialBit.GEAR, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEAR_TRANSFORMER)
                .withBit(MaterialBit.PLATE, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.PLATE_TRANSFORMER)
                .build();
        
        // Other
        COAL_COKE = new Material.Builder(Ids.COAL_COKE)
                .withBit(MaterialBit.MATTER, ItemSuppliers.MATERIAL_ITEM.get(), NameTransformer.GEM_TRANSFORMER)
                .withBit(MaterialBit.STORAGE_BLOCK, BlockSuppliers.metalBlock(), NameTransformer
                        .STORAGE_BLOCK_TRANSFORMER)
                .build();
    }


    public static void registerAll() {
        Matter.LOGGER.debug("[Materials] Registering Vanilla Materials.");
        register( Ids.COAL, COAL);
        register( Ids.IRON, IRON);
        register( Ids.GOLD, GOLD);
        register( Ids.DIAMOND, DIAMOND);
        register( Ids.QUARTZ, QUARTZ);
        register( Ids.REDSTONE, REDSTONE);
        register( Ids.LAPIS, LAPIS);
        register( Ids.EMERALD, EMERALD);
        // Here for the future :)
        //register(Ids.NETHERITE, NETHERITE);

        // Metals
        register( Ids.COPPER, COPPER);
        register( Ids.TIN, TIN);
        register( Ids.ALUMINUM, ALUMINUM);
        register( Ids.SILVER, SILVER);
        register( Ids.PLATINUM, PLATINUM);
        register( Ids.LEAD, LEAD);
        register( Ids.ZINC, ZINC);
        register( Ids.TUNGSTEN, TUNGSTEN);
        register( Ids.COBALT, COBALT);
        register( Ids.OSMIUM, OSMIUM);
        register( Ids.PALLADIUM, PALLADIUM);
        register( Ids.TITANIUM, TITANIUM);
        register( Ids.URANIUM, URANIUM);
        register( Ids.THORIUM, THORIUM);
        register( Ids.PLUTONIUM, PLUTONIUM);
        register( Ids.IRIDIUM, IRIDIUM);

        // Gems
        register( Ids.RUBY, RUBY);
        register( Ids.SAPPHIRE, SAPPHIRE);
        register( Ids.PERIDOT, PERIDOT);
        register( Ids.QUICKSILVER, QUICKSILVER);
        register( Ids.AMETHYST, AMETHYST);
        register( Ids.TOPAZ, TOPAZ);

        // Alloys
        register( Ids.STEEL, STEEL);
        register( Ids.BRONZE, BRONZE);
        register( Ids.BRASS, BRASS);
        register( Ids.ELECTRUM, ELECTRUM);
        register( Ids.ROSE_GOLD, ROSE_GOLD);

        // Other
        register( Ids.COAL_COKE, COAL_COKE);
    }

    /**
     * Built-in Material IDs.
     */
    public static class Ids {
        public static final Identifier COAL = new Identifier("minecraft", "coal");
        public static final Identifier IRON = new Identifier("minecraft", "iron");
        public static final Identifier GOLD = new Identifier("minecraft", "gold");
        public static final Identifier DIAMOND = new Identifier("minecraft", "diamond");
        public static final Identifier QUARTZ = new Identifier("minecraft", "quartz");
        public static final Identifier REDSTONE = new Identifier("minecraft", "redstone");
        public static final Identifier LAPIS = new Identifier("minecraft", "lapis");
        public static final Identifier EMERALD = new Identifier("minecraft","emerald");
        // Here for the future :)
        //public static final Identifier NETHERITE = new Identifier("minecraft","netherite");

        // Metals
        public static final Identifier COPPER = new Identifier("c", "copper");
        public static final Identifier TIN = new Identifier("c", "tin");
        public static final Identifier ALUMINUM = new Identifier("c", "aluminum");
        public static final Identifier SILVER = new Identifier("c", "silver");
        public static final Identifier PLATINUM = new Identifier("c","platinum");
        public static final Identifier PALLADIUM = new Identifier("c","palladium");
        public static final Identifier OSMIUM = new Identifier("c","osmium");
        public static final Identifier COBALT = new Identifier("c","cobalt");
        public static final Identifier LEAD = new Identifier("c", "lead");
        public static final Identifier ZINC = new Identifier("c", "zinc");
        public static final Identifier TUNGSTEN = new Identifier("c", "tungsten");
        public static final Identifier TITANIUM = new Identifier("c", "titanium");
        public static final Identifier URANIUM = new Identifier("c", "uranium");
        public static final Identifier THORIUM = new Identifier("c", "thorium");
        public static final Identifier IRIDIUM = new Identifier("c","iridium");
        public static final Identifier PLUTONIUM = new Identifier("c","plutonium");

        // Gems
        public static final Identifier RUBY = new Identifier("c", "ruby");
        public static final Identifier SAPPHIRE = new Identifier("c", "sapphire");
        public static final Identifier PERIDOT = new Identifier("c", "peridot");
        public static final Identifier QUICKSILVER = new Identifier("c", "quicksilver");
        public static final Identifier AMETHYST = new Identifier("c", "amethyst");
        public static final Identifier TOPAZ = new Identifier("c", "topaz");

        // Alloys
        public static final Identifier STEEL = new Identifier("c", "steel");
        public static final Identifier BRONZE = new Identifier("c", "bronze");
        public static final Identifier BRASS = new Identifier("c", "brass");
        public static final Identifier ELECTRUM = new Identifier("c", "electrum");
        public static final Identifier ROSE_GOLD = new Identifier("c","rose_gold");

        // Other
        public static final Identifier COAL_COKE = new Identifier("c","coal_coke");
    }

}
