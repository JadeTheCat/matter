package me.jadethecat.matter.tag;

import com.google.common.util.concurrent.MoreExecutors;
import me.jadethecat.matter.Matter;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.resource.ResourceManager;
import net.minecraft.tag.RegistryTagContainer;
import net.minecraft.tag.Tag;
import net.minecraft.tag.TagContainer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.Map;

public class WorldTagReloadListener implements SimpleSynchronousResourceReloadListener {
    public static final Identifier ID = new Identifier(Matter.MODID, "world_tags");

    @Override
    public void apply(ResourceManager resourceManager) {
        BiomeTags.setContainer(reload(resourceManager, Registry.BIOME, "tags/biomes", "biome"));
        DimensionTypeTags.setContainer(reload(resourceManager, Registry.DIMENSION_TYPE, "tags/dimensions", "dimension"));
    }

    private static <T> TagContainer<T> reload(ResourceManager resourceManager, Registry<T> registry, String folder, String name) {
        RegistryTagContainer<T> container = new RegistryTagContainer<>(registry, folder, name);

        try {
            Map<Identifier, Tag.Builder<T>> map = container.prepareReload(resourceManager, MoreExecutors.directExecutor()).get(); //waits synchronously for prepareReload to complete
            container.applyReload(map);
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return container;
    }

    @Override
    public Identifier getFabricId() {
        return ID;
    }
}
