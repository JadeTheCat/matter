package me.jadethecat.matter.tag;

import me.jadethecat.matter.Matter;
import net.fabricmc.fabric.api.tag.TagRegistry;
import net.minecraft.block.Block;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public class MatterTags {
    public static final Tag<Block> NATURAL_STONES = TagRegistry.block(new Identifier(Matter.COMMON, "natural_stones"));
}
