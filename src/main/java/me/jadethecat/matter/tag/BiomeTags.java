package me.jadethecat.matter.tag;

import net.minecraft.tag.TagContainer;
import net.minecraft.world.biome.Biome;

import java.util.Optional;

public class BiomeTags {
    public static TagContainer<Biome> CONTAINER = new TagContainer<>((identifier) -> {
        return Optional.empty();
    }, "", false, "");

    public static TagContainer<Biome> getContainer() {
        return CONTAINER;
    }

    static void setContainer(TagContainer<Biome> container) {
        CONTAINER = container;
    }
}
