package me.jadethecat.matter.tag;

import net.minecraft.tag.TagContainer;
import net.minecraft.world.dimension.DimensionType;

import java.util.Optional;

public class DimensionTypeTags {
    public static TagContainer<DimensionType> CONTAINER = new TagContainer<>((identifier) -> Optional.empty(), "", false, "");

    public static TagContainer<DimensionType> getContainer() {
        return CONTAINER;
    }

    static void setContainer(TagContainer<DimensionType> container) {
        CONTAINER = container;
    }
}
