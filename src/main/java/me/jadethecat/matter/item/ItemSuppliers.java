package me.jadethecat.matter.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

import java.util.function.Supplier;


public class ItemSuppliers {
    public static final Supplier<Item> MATERIAL_ITEM = () -> new Item(new Item.Settings()
            .group(ItemGroup.MATERIALS)
            .maxCount(64)
    );
}
