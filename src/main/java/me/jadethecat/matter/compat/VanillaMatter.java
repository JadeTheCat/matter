/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.compat;

import me.jadethecat.matter.api.material.MaterialBit;
import me.jadethecat.matter.api.MatterInitializer;
import me.jadethecat.matter.api.material.MaterialRequestManager;
import me.jadethecat.matter.material.Materials;

/**
 * Vanilla compatibility module for Matter.
 */
public class VanillaMatter implements MatterInitializer {
    private static final String MCMODID = "minecraft";
    @Override
    public void initRequests(MaterialRequestManager manager) {
        manager.request(MCMODID, Materials.Ids.COAL, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.IRON, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.NUGGET, 
                MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.GOLD, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.NUGGET, 
                MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.DIAMOND, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.REDSTONE, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.DUST,
                MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.QUARTZ, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.LAPIS, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.STORAGE_BLOCK);
        manager.request(MCMODID, Materials.Ids.EMERALD, MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.STORAGE_BLOCK);
    }

    @Override
    public void initMaterials() {

    }
}
