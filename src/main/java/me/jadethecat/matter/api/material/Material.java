/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.api.material;

import me.jadethecat.matter.util.NameTransformer;
import net.minecraft.block.Block;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

import java.util.*;
import java.util.function.Supplier;

public class Material {
    public Set<String> requestedBy = new HashSet<>();
    Map<Identifier, MaterialBit<?>> bits = new HashMap<>();

    public Optional<MaterialBit<?>> getBitOrEmpty(Identifier id) {
        MaterialBit<?> bit = bits.getOrDefault(id, null);
        return bit != null ? Optional.of(bit) : Optional.empty();
    }

    public Map<Identifier, MaterialBit<?>> getBits() {
        return bits;
    }

    /**
     * Set what bits are enabled. Used internally in the Registry.
     * @param bitsRequested The bits that have been requested.
     */
    public void setBitsEnabled(Set<Identifier> bitsRequested) {
        bits.forEach((id, bit) -> {
            if (bitsRequested.contains(id))
                bit.enabled = true;
        });
    }

    /**
     * A builder for the Material class, for making materials easier.
     */
    public static class Builder {
        private Material material;
        private Identifier id;
        public Builder(Identifier id) {
            this.material = new Material();
            this.id = id;
        }
        public <T> Builder withBit(Identifier id, T bit, NameTransformer nameTransformer) {
            MaterialBit<T> materialBit = MaterialBit.of(bit)
                    .withId(id)
                    .forMaterial(this.id)
                    .withTransformer(nameTransformer);
            this.material.bits.putIfAbsent(id, materialBit);
            return this;
        }

        public Material build() {
            return this.material;
        }
    }


}
