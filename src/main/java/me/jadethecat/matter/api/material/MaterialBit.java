/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.api.material;

import me.jadethecat.matter.util.NameTransformer;
import net.minecraft.block.Block;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

public class MaterialBit<T> {
    public boolean enabled = false;
    private T bit;
    private Identifier bitId;
    private Identifier materialId;
    private NameTransformer nameTransformer = new NameTransformer();

    public MaterialBit(T bit) {
        this.bit = bit;
    }

    public static <B> MaterialBit<B> of(B bit) {
        return new MaterialBit<>(bit);
    }

    public T get() {
        return bit;
    }

    public NameTransformer getNameTransformer() {
        return nameTransformer;
    }

    public Identifier getTransformedId() {
        return new Identifier(materialId.getNamespace(),
                nameTransformer.transform(materialId.getPath()));
    }
    public Identifier getBitId() {
        return bitId;
    }

    MaterialBit<T> withTransformer(NameTransformer nameTransformer) {
        this.nameTransformer = nameTransformer;
        return this;
    }
    MaterialBit<T> forMaterial(Identifier materialId) {
        this.materialId = materialId;
        return this;
    }
    MaterialBit<T> withId(Identifier bitId) {
        this.bitId = bitId;
        return this;
    }

    public static class Builder<T> {
        MaterialBit<T> bit;
        public Builder(T bit) {
            this.bit = new MaterialBit<>(bit);
        }
        public Builder<T> withTransformer(NameTransformer nameTransformer) {
            this.bit.nameTransformer = nameTransformer;
            return this;
        }
        public Builder<T> forMaterial(Identifier materialId) {
            this.bit.materialId = materialId;
            return this;
        }
        public Builder<T> withId(Identifier bitId) {
            this.bit.bitId = bitId;
            return this;
        }
        public MaterialBit<T> build() {
            return bit;
        }
    }


    public static final Identifier MATTER = new Identifier("c", "matter");
    public static final Identifier ORE = new Identifier("c","ore");
    public static final Identifier NETHER_ORE = new Identifier("c","nether_ore");
    public static final Identifier END_ORE = new Identifier("c","end_ore");
    public static final Identifier STORAGE_BLOCK = new Identifier("c","block");
    public static final Identifier DUST = new Identifier("c", "dust");
    public static final Identifier NUGGET = new Identifier("c","nugget");
    public static final Identifier PLATE = new Identifier("c","plate");
    public static final Identifier GEAR = new Identifier("c","gear");
}
