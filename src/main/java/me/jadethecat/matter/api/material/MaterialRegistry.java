/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.api.material;

import me.jadethecat.matter.Matter;
import me.jadethecat.matter.MatterData;
import me.jadethecat.matter.block.MippedOreBlock;
import me.jadethecat.matter.util.NameTransformer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.SimpleRegistry;

import java.util.*;

/**
 * Registry for Materials, custom and not.
 */
public class MaterialRegistry extends SimpleRegistry<Material> {
    public static final Identifier MATERIAL_REGISTRY_ID = new Identifier(Matter.MODID, "material");
    public static final MaterialRegistry MATERIAL =
            new MaterialRegistry();

    private List<Identifier> handledBitIds = Arrays.asList(MaterialBit.ORE, MaterialBit.MATTER, MaterialBit.NETHER_ORE,
            MaterialBit.END_ORE, MaterialBit.DUST, MaterialBit.NUGGET, MaterialBit.STORAGE_BLOCK, MaterialBit.PLATE,
            MaterialBit.GEAR);

    @Override
    public <V extends Material> V add(Identifier id, V entry) {
        Matter.LOGGER.debug("[MaterialRegistry] Registering material {}", id.toString());
        entry.bits = MaterialBitCallback.EVENT.invoker().addBits(id, entry.bits);
        entry.requestedBy = MaterialRequestManager.INSTANCE.getRequesterMods(id);
        Set<Identifier> bitsList = MaterialRequestManager.INSTANCE.getBits(id);
        Matter.CONFIG.enabledBits.getOrDefault(id.toString(), new HashSet<>()).forEach(bit -> {
            bitsList.add(new Identifier(bit));
        });
        List<MaterialRequest> staticRequests = MatterData.getStaticRequests(id);
        for (MaterialRequest req : staticRequests) {
            bitsList.addAll(req.getBitsRequested());
            entry.requestedBy.addAll(req.getModids());
        }
        Matter.LOGGER.debug("[MaterialRegistry] Mods that have requested {} : {}", id.toString(),
                entry.requestedBy.toString());
        Matter.LOGGER.debug("[MaterialRegistry] Requested bits for {} : {}", id.toString(),
                bitsList.toString());
        entry.setBitsEnabled(bitsList);
        registerBits(id, entry);
        return super.add(id, entry);
    }

    private <V extends Material> void registerBits(Identifier id, V mat) {
        Matter.LOGGER.debug("[MaterialRegistry] Registering bits for material {}", id.toString());
        for (Map.Entry<Identifier,MaterialBit<?>> bitEntry : mat.getBits().entrySet()) {
            Identifier bitId = bitEntry.getKey();
            if (handledBitIds.contains(bitId) && bitEntry.getValue() != null) {
                MaterialBit<?> bit = bitEntry.getValue();
                if (bit.get() instanceof Item && !Registry.ITEM.getOrEmpty(bit.getTransformedId()).isPresent()) {
                    registerItem(changeMinecraftToCommonId(bit.getTransformedId()), (Item)bit.get());
                } else if (bit.get() instanceof Block && !Registry.BLOCK.getOrEmpty(bit.getTransformedId()).isPresent()) {
                    registerBlock(changeMinecraftToCommonId(bit.getTransformedId()), (Block)bit.get());
                }
            }
        }
    }
    private Identifier changeMinecraftToCommonId(Identifier id) {
        return new Identifier(id.getNamespace().replaceAll("minecraft", "c"), id.getPath());
    }
    private void registerBlock(Identifier id, Block block) {
        if (block instanceof MippedOreBlock && FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT)
            handleMipped(block);
        Registry.register(Registry.BLOCK, id, block);
        registerItem(id, new BlockItem(block, new BlockItem.Settings().group(ItemGroup.BUILDING_BLOCKS)));
    }
    private void registerItem(Identifier id, Item item) {
        Registry.register(Registry.ITEM, id, item);
    }
    @Environment(EnvType.CLIENT)
    private static void handleMipped(Block block) {
        BlockRenderLayerMap.INSTANCE.putBlock(block, RenderLayer.getCutoutMipped());
    }
}
