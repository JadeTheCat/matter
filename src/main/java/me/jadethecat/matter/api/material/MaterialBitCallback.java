/*
 * MIT License
 *
 * Copyright (c) 2020 JadeTheCat
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package me.jadethecat.matter.api.material;

import me.jadethecat.matter.Matter;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.util.Identifier;

import java.util.Map;

public interface MaterialBitCallback {
    Event<MaterialBitCallback> EVENT = EventFactory.createArrayBacked(MaterialBitCallback.class, (listeners) -> (id, bits) -> {
        Matter.LOGGER.debug("[MaterialBitCallback] Performing bit callbacks for {}", id.toString());
        for (MaterialBitCallback listener : listeners) {
            Map<Identifier,MaterialBit<?>> newBits = listener.addBits(id, bits);
            for (Map.Entry<Identifier,MaterialBit<?>> bit : newBits.entrySet()) {
                bits.putIfAbsent(bit.getKey(), bit.getValue());
            }
        }
        return bits;
    });

    /**
     * Add bits to an existing Material. Can only add, never replace.
     * @param id The Identifier of the Material.
     * @param bits The current bits of the Material.
     * @return The bits you wish to add to the Material.
     */
    Map<Identifier,MaterialBit<?>> addBits(Identifier id, Map<Identifier,MaterialBit<?>> bits);
}
