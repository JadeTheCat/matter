package me.jadethecat.matter.api.world;

import net.minecraft.block.BlockState;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.OreFeatureConfig;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class OreGenConfig {
    public int oreVeinSize;
    public int veinsPerChunk;
    public int minY;
    public int maxY;
    public OreFeatureConfig.Target target;
    public Set<Identifier> biomeWhitelist = new HashSet<>();
    public Set<Identifier> biomeBlacklist = new HashSet<>();
    public Identifier materialId;
    public Identifier bitId;

    public OreGenConfig oreVeinSize(int oreVeinSize) {
        this.oreVeinSize = oreVeinSize;
        return this;
    }
    public OreGenConfig veinsPerChunk(int veinsPerChunk) {
        this.veinsPerChunk = veinsPerChunk;
        return this;
    }
    public OreGenConfig yRange(int min, int max) {
        this.minY = min;
        this.maxY = max;
        return this;
    }
    public OreGenConfig target(OreFeatureConfig.Target target) {
        this.target = target;
        return this;
    }
    public OreGenConfig ids(Identifier materialId, Identifier bitId) {
        this.materialId = materialId;
        this.bitId = bitId;
        return this;
    }
    public OreGenConfig whitelist(Set<Identifier> biomeWhitelist) {
        this.biomeWhitelist = biomeWhitelist;
        return this;
    }
    public OreGenConfig blacklist(Set<Identifier> biomeBlacklist) {
        this.biomeBlacklist = biomeBlacklist;
        return this;
    }

    public static Set<Identifier> OVERWORLD_BIOMES = new HashSet<>();
    public static Set<Identifier> NETHER_BIOMES = new HashSet<>();
    public static Set<Identifier> END_BIOMES = new HashSet<>();
    public static Set<Identifier> NON_OVERWORD_BIOMES = new HashSet<>();
}
