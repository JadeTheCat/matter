package me.jadethecat.matter.block;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tools.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;

import java.awt.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class BlockSuppliers {
    public static Block tierZeroOre(int xpMin, int xpMax) {
        return new MippedOreBlock(FabricBlockSettings.of(Material.STONE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 0)
                .build(),
                xpMin,
                xpMax
        );
    }
    public static Block tierOneOre(int xpMin, int xpMax) {
        return new MippedOreBlock(FabricBlockSettings.of(Material.STONE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 1)
                .build(),
                xpMin,
                xpMax
        );
    }
    public static Block tierOneLightableOre(int xpMin, int xpMax, Color particleColor) {
        return new LightableMippedOreBlock(FabricBlockSettings.copy(Blocks.REDSTONE_ORE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 1)
                .build(),
                xpMin,
                xpMax,
                particleColor
        );
    }
    public static Block tierTwoOre(int xpMin, int xpMax) {
        return new MippedOreBlock(FabricBlockSettings.of(Material.STONE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 2)
                .build(),
                xpMin,
                xpMax
        );
    }
    public static Block tierTwoLightableOre(int xpMin, int xpMax, Color particleColor) {
        return new LightableMippedOreBlock(FabricBlockSettings.copy(Blocks.REDSTONE_ORE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 2)
                .build(),
                xpMin,
                xpMax,
                particleColor
        );
    }
    public static Block tierThreeOre(int xpMin, int xpMax) {
        return new MippedOreBlock(FabricBlockSettings.of(Material.STONE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 3)
                .build(),
                xpMin,
                xpMax
        );
    }
    public static Block tierThreeLightableOre(int xpMin, int xpMax, Color particleColor) {
        return new LightableMippedOreBlock(FabricBlockSettings.copy(Blocks.REDSTONE_ORE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 3)
                .build(),
                xpMin,
                xpMax,
                particleColor
        );
    }
    public static Block tierFourOre(int xpMin, int xpMax) {
        return new MippedOreBlock(FabricBlockSettings.of(Material.STONE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 4)
                .build(),
                xpMin,
                xpMax
        );
    }
    public static Block tierFourLightableOre(int xpMin, int xpMax, Color particleColor) {
        return new LightableMippedOreBlock(FabricBlockSettings.copy(Blocks.REDSTONE_ORE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 4)
                .build(),
                xpMin,
                xpMax,
                particleColor
        );
    }
    public static Block tierFiveOre(int xpMin, int xpMax) {
        return new MippedOreBlock(FabricBlockSettings.of(Material.STONE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 5)
                .build(),
                xpMin,
                xpMax
        );
    }
    public static Block tierFiveLightableOre(int xpMin, int xpMax, Color particleColor) {
        return new LightableMippedOreBlock(FabricBlockSettings.copy(Blocks.REDSTONE_ORE)
                .hardness(3.0f)
                .resistance(3.0f)
                .breakByTool(FabricToolTags.PICKAXES, 5)
                .build(),
                xpMin,
                xpMax,
                particleColor
        );
    }

    public static Block metalBlock() {
        return new Block(FabricBlockSettings
                .of(Material.METAL)
                .hardness(5.0f)
                .resistance(6.0f)
                .build()
        );
    }
    public static Block stoneBlock() {
        return new Block(FabricBlockSettings
                .of(Material.STONE)
                .hardness(5.0f)
                .resistance(6.0f)
                .build()
        );
    }
}
