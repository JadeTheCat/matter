package me.jadethecat.matter.block;

import me.jadethecat.matter.Matter;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplier;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.OreBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTables;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MippedOreBlock extends OreBlock {
    private boolean complainedAboutLoot = false;
    private boolean hasXp = false;
    private int xpMin;
    private int xpMax;

    public MippedOreBlock(Settings settings, int xpMin, int xpMax) {
        super(settings);
        this.xpMin = xpMin;
        this.xpMax = xpMax;
        hasXp = (!(xpMin == 0 && xpMax == 0) && xpMin >= 0 && xpMax >= 0 && xpMax >= xpMin);
    }

    @Override
    protected int getExperienceWhenMined(Random random) {
        return hasXp ? MathHelper.nextInt(random, xpMin, xpMax) : 0;
    }

    @Override
    public List<ItemStack> getDroppedStacks(BlockState state, LootContext.Builder builder) {
        //EARLY DETECTION OF BUSTED LOOT TABLES:
        Identifier tableId = this.getDropTableId();

        if (tableId == LootTables.EMPTY) {
            return Collections.emptyList();
        } else {
            LootContext context = builder.put(LootContextParameters.BLOCK_STATE, state).build(LootContextTypes.BLOCK);
            ServerWorld world = context.getWorld();
            LootTable lootSupplier = world.getServer().getLootManager().getSupplier(tableId);

            List<ItemStack> result = lootSupplier.getDrops(context);

            if (result.isEmpty()) {
                //This might not be good. Confirm:

                if (lootSupplier instanceof FabricLootSupplier) {
                    List<LootPool> pools = ((FabricLootSupplier) lootSupplier).getPools();

                    if (pools.isEmpty()) {
                        //Yup. Somehow we got a loot pool that just never drops anything.
                        if (!complainedAboutLoot) {
                            Matter.LOGGER.error("Loot pool '" + tableId + "' doesn't seem to be able to drop anything. Supplying the ore block instead. Please report this!");
                            complainedAboutLoot = true;
                        }

                        result.add(new ItemStack(this.asItem()));
                    }
                }
            }

            return result;
        }
    }

}
