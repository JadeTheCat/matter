# Matter

A library for adding materials easier.

Materials, in this context, is the base of any crafting recipe. Iron, Gold, Diamond, Copper, and Tin are all examples 
of materials, as are Stone, Marble, and Basalt.

Every item and block related to these materials which is seen as a "crafting item" is known in Matter as a bit.

See the wiki for more usage information.